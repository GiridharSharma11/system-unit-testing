"""
FIT-2107 Assignment3
Team Name: Tom and Jerry 
Author 1: Thuta Lin(ID: 28729994)
Author 2: Giridhar Gopal Sharma(ID: )
Last Modified Time: 18/10/2018 
"""

import unittest
from unittest.mock import Mock,patch
from scheduler import Scheduler
from scheduler import IllegalArgumentException
import datetime
from datetime import datetime 
from skyfield.api import Loader, Topos
import scheduler

class SchedulerTest(unittest.TestCase):
    '''Tests for the scheduler class.  Add more tests
    to test the code that you write'''

    def setUp(self):
        self.scheduler = Scheduler()

    #This commented test case was given by the lecturer
    """
    def test_itsalive(self):
        (stime, satellites) = Scheduler.find_time(self.scheduler)
        self.assertTrue(type(stime)==type(datetime.now()))
        self.assertTrue(satellites[0]=="ISS (ZARYA)")
        self.assertTrue(satellites[1]=="COSMOS-123")
    """

    #This test case was given by the lecturer
    def test_exceptionthrown(self):
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(start_time = "now")

    def test_exceptionthrown_find_time_BB001(self):
        """
        TestID: Test_BB_001
        Testing for handling invalid input parameters
        TestFrame: { valid url, invalid datetime, n_windows>0, invalid duration, invalid sample_interval, True, 
        tuple( invalid float -180<x<180 and float type)}
        Expected result: Raise TypeError as input argument for datetime,type is of invalid
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements/visual.txt', '8:30:24', 24, "60", -10, True, (-91.86734,150))

    def test_exceptionthrown_find_time_BB002(self):
        """
        TestID: Test_BB_002
        Testing for handling invalid input parameters
        TestFrame: { invalid url, valid datetime, negative n_windows value, duration(string input),
        valid interval, False, tuple( invalid datatypes inputted)}
        Expected result: Raise ValueError as input argument for n_windows is of invalid(negative) value
        """
        with self.assertRaises(ValueError):
            (stime, satellites) = self.scheduler.find_time('https://docs.google.com', datetime.now(), -1, 60, "2", False, ["90","90"])

    def test_exceptionthrown_find_time_BB003(self):
        """
        TestID: Test_BB_003
        Testing for handling invalid input parameters
        TestFrame: { invalid url, valid datetime, invalid n_windows, duration>0, invalid type for interval,
        not boolean,  tuple( -90<x<90 and not float type) }
        Expected result: Raise TypeError as input argument for n_windows is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(100, datetime.now(), 3.5, 60, "2", "True", (-80.4475847, 190.54875847))

    def test_exceptionthrown_find_time_BB004(self):
        """
        TestID: Test_BB_004
        Testing for handling invalid input parameters
        TestFrame: { invalid url, invalid datetime, invalid type for n_windows, duration>0, 
        valid interval, False, tuple(invalid datatypes, not float type) }
        Expected result: Raise TypeError as input argument for start_time is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements/my_test_visual.txt', '8:30:24', "30", 60, 1, False, ["90", 180.8888])
    
    def test_exceptionthrown_find_time_BB005(self):
        """
        TestID: Test_BB_005
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, invalid n_window, invalid duration, invalid interval,
        not boolean, tuple( invalid type, -180<x<180 and float type) }
        Expected result: Raise TypeError as input argument for n_windows is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements/visual.txt', datetime.now(), "30", "60", 1.55, "False", ["90", 170.4737483])

    def test_exceptionthrown_find_time_BB006(self):
        """
        TestID: Test_BB_006
        Testing for handling invalid input parameters
        TestFrame: { invalid url, invalid datetime, invalid n_window, duration>0, 
        invalid interval, True, tuple(-90<x<90 and float, invalid datatype) }
        Expected result: Raise TypeError as input argument for start_time is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(100, '8:30:24', -1, 60, -2, True, (70.67364538, 170))
    
    def test_exceptionthrown_find_time_BB007(self):
        """
        TestID: Test_BB_007
        Testing for handling invalid input parameters
        TestFrame: { invalid url, invalid datetime, invalid n_window, duration>0, invalid interval,
        not boolean, tuple( latitude = invalid float, longitude = invalid float) }
        Expected result: Raise TypeError as input argument for start_time is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time('https://docs.google.com', '8:30:24', "24", 60, -2, "True", "(-100.74738, -200.000)")

    def test_exceptionthrown_find_time_BB008(self):
        """
        TestID: Test_BB_008
        Testing for handling invalid input parameters
        TestFrame: {valid url, valid datetime, invalid n_window, invalid duration, invalid interval, True, tuple(
        lat = not float, long = not float type )}
        Expected result: Raise ValueError as input arguments for duration and n_windows is of
        invalid datatype
        """
        with self.assertRaises(ValueError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements/visual.txt', datetime.now(), -10, -60, 2, True, "(-90.67364538, -160.443453)")

    def test_exceptionthrown_find_time_BB009(self):
        """
        TestID: Test_BB_009
        Testing for handling invalid input parameters
        TestFrame: { invalid url, valid datetime, valid n_windows, invalid duration, invalid interval, False,
        tuple(lat = -90<x<90 and float, long= invalid float) }
        Expected result: Raise TypeError as input arguments for duration and sample interval is of
        invalid(negative) value
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(4.33, datetime.now(), 24, "60", "2", False, (-85.4534533, -190.475387))
    
    def test_exceptionthrown_find_time_BB010(self):
        """
        TestID: Test_BB_010
        Testing for handling invalid input parameters
        TestFrame: { valid url, invalid datetime, valid n_windows, invalid duration, invalid interval,
        not boolean, tuple(lat=invalid float, long = invalid datatype) }
        Expected result: Raise TypeError as input arguments for datetime interval is of
        invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements/visual.txt', '8:30:24', 24, "60", "2", 0, (-95.4534533, "190.475387"))

    def test_exceptionthrown_find_time_BB011(self):
        """
        TestID: Test_BB_011
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, invalid n_windows, valid duration, 
        invalid value, False, Tuple( lat=invalid datatype, long= invalid float) }
        Expected result: Raise ValueError as input arguments for n_windows interval is of
        invalid(negative) value
        """
        with self.assertRaises(ValueError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements/visual.txt', datetime.now(), -24, 60, -2, False, ("-95.4534533", 190.475387))

    def test_exceptionthrown_find_time_BB012(self):
        """
        TestID: Test_BB_012
        Testing for handling invalid input parameters
        TestFrame: { invalid url, invalid datetime, invalid n_window, invalid duration,
        valid interval, False, tuple( lat=-90<x<90 and float, long = invalid  }
        Expected result: Raise TypeError as input arguments for start_time is of
        invalid datatype
        """        
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements', '8:30:24', "24", "60", 1, False, (75.4534533, 190))
    
    def test_exceptionthrown_find_time_BB013(self):
        """
        TestID: Test_BB_013
        Testing for handling invalid input parameters
        TestFrame: { invalid url, valid datetime, valid n_windows, invalid duration, invalid interval, True, 
        tuple( lat = invalid datatype, long = -180<x<180 and float type)}
        Expected result: Raise TypeError as input arguments for duration and
        sample_interval is of invalid datatype
        """        
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(100, datetime.now(), 24, "60", -1, True, ("75.4534533", 150.734758374))
    
    def test_exceptionthrown_find_time_BB014(self):
        """
        TestID: Test_BB_014
        Testing for handling invalid input parameters
        TestFrame: { valid url, invalid datetime, valid n_windows, valid duration, valid interval, true, 
        tuple( lat= -90<x<90 and float, long= -180<x<180 and float type)}
        Expected result: Raise TypeError as for start_time is of invalid datatype
        """        
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time('http://celestrak.com/NORAD/elements/visual.txt', '8:30:24', 24, 60, 1, True, [75.4534533, 150.734758374])


    def test_exceptionthrown_find_time_BB015(self):
        """
        TestID: Test_BB_015
        Testing for handling invalid input parameters
        TestFrame: {invalid url, valid datetime, valid n_window, valid duration, valid interval,
        not boolean, tuple(Lat = -90<x<90 and float, long = -180<x<180 and float type) }
        Expected result: Raise ValueError as for cumulative is of not boolean type
        """
        with self.assertRaises(TypeError): 
            (stime, satellites) = self.scheduler.find_time('http://celestar.com/NORAD/elements/visual.txt', datetime.now(), 24, 60, 1, "True", (75.4534533, -150.734758374))

    def test_exceptionthrown_find_time_BB016(self):
        """
        TestID: Test_BB_016
        Testing for handling invalid input parameters
        TestFrame: { invalid url, valid datetime, invalid n_windows, valid duration,
        valid interval, true, tuple(lat=invalid float, long=invalid float) }
        Expected result: Raise TypeError as for n_windows the value is of invalid
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(69, datetime.now(), "24", 60, 3, True, [-105.4534533, -190.734758374])

    def test_exceptionthrown_find_time_BB017(self):
        """
        TestID: Test_BB_017
        Testing for handling invalid input parameters
        TestFrame: { invalid url, valid datetime, invalid n_window, invalid duration, valid interval, 
        not boolean, tuple( lat = invalid datatype, long = -180<x<180 and float type) }
        Expected result: Raise TypeError as for url the value is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(68, datetime.now(), -24, "60", 2, 10, [105, 120.734758374])


    def test_exceptionthrown_find_time_WB001(self):
        """
        TestID: Test_WB_001
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid(negative) value, valid value, False, 
        tuple( -90<x<90 and float type, -180<x<180 and float type) }
        Expected result: Raise ValueError as duration is of invalid(negative) value
        """
        with self.assertRaises(ValueError):
            (stime, satellites) = self.scheduler.find_time(duration=-60)

    def test_exceptionthrown_find_time_WB002(self):
        """
        TestID: Test_WB_002
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid type, valid value, False, 
        tuple( -90<x<90 and float type, -180<x<180 and float type) }
        Expected result: Raise TypeError as for duration the value is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(duration=60.783242973)
    
    def test_exceptionthrown_find_time_WB003(self):
        """
        TestID: Test_WB_003
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, integer > 0,  integer > 0, invalid datatype, False, 
        tuple( -90<x<90 and float type, -180<x<180 and float type) }
        Expected result: Raise TypeError as for sample_interval the value is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(sample_interval="1")

    def test_exceptionthrown_find_time_WB004(self):
        """
        TestID: Test_WB_004
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0,  integer > 0, invalid(negative) integer, False, 
        tuple( -90<x<90 and float type, -180<x<180 and float type) }
        Expected result: Raise ValueError as for sample_interval the value is of invalid(negative) value
        """
        with self.assertRaises(ValueError):
            (stime, satellites) = self.scheduler.find_time(sample_interval=-1)

    def test_exceptionthrown_find_time_WB005(self):
        """
        TestID: Test_WB_005
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid type, valid value, False, 
        tuple( invalid datatype, -180<x<180 and float type)}
        Expected result: Raise TypeError as for lat value is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(location=("hello",-40.47834))

    def test_exceptionthrown_find_time_WB006(self):
        """
        TestID: Test_WB_006
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid type, valid value, False, 
        invalid float type, -180<x<180 and float type) }
        Expected result: Raise TypeError as for lat value is of invalid float value
        """
        with self.assertRaises(ValueError):
            (stime, satellites) = self.scheduler.find_time(location=(100.457834,-40.47834))

    def test_exceptionthrown_find_time_WB007(self):
        """
        TestID: Test_WB_007
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid type, valid value, False, 
        tuple( -90<x<90 and float type, invalid datatype)}`
        Expected result: Raise TypeError as for long value is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(location=(80.457834,"-40.47834"))

    def test_exceptionthrown_find_time_WB008(self):
        """
        TestID: Test_WB_008
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid type, valid value, False, 
        tuple( -90<x<90 and float type, invalid float type) }
        Expected result: Raise ValueError as for long value is of invalid float value
        """
        with self.assertRaises(ValueError):
            (stime, satellites) = self.scheduler.find_time(location=(80.457834,190.47834))

    def test_exceptionthrown_find_time_WB009(self):
        """
        TestID: Test_WB_009
        Testing for handling invalid input parameters
        TestFrame: { invalid url, valid datetime, int > 0, invalid type, valid value, False, 
        tuple( -90<x<90 and float type, -180<x<180 and float type) }
        Expected result: Raise IllegalArgumentexception as for url is of invalid
        """
        with self.assertRaises(IllegalArgumentException):
            (stime, satellites) = self.scheduler.find_time(satlist_url = 'http://google.com', location=(80.457834, -170.47834))
    
    def test_exceptionthrown_fine_time_WB010(self):
        """
        TestID: Test_WB_010
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid type, valid value, False, not tuple}
        Expected result: Raise TypeError as location is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(location="London")

    def test_exceptionthrown_fine_time_WB011(self):
        """
        TestID: Test_WB_011
        Testing for handling invalid input parameters
        TestFrame: { valid url, valid datetime, int > 0, invalid type, valid value, not boolean, 
        tuple( -90<x<90 and float type, -180<x<180 and float type) }
        Expected result: Raise TypeError as input for cumulative is of invalid datatype
        """
        with self.assertRaises(TypeError):
            (stime, satellites) = self.scheduler.find_time(cumulative=1)
    
    @patch('scheduler.Scheduler.get_user_location_Topos')
    @patch('scheduler.Scheduler.find_time_interval_set')
    @patch('scheduler.Scheduler.get_satellites')
    @patch('scheduler.Scheduler.get_visible_satellites')
    def test_cumulative_find_time_WB012(self, visible_mock,sat_mock,time_mock, topos_mock):
        """
        TestID: Test_WB_012
        Testing and mocking for calling Schedule.find_time()
        """
        my_scheduler = Scheduler()
        topos_mock.return_value = 2
        self.assertEqual(my_scheduler.get_user_location_Topos(1), 2)
        time_mock.return_value = [(datetime(2017,4,10,2,2,20),datetime(2017,4,10,3,2,20))]
        self.assertEqual(my_scheduler.find_time_interval_set(1,2,3),[(datetime(2017,4,10,2,2,20),datetime(2017,4,10,3,2,20))])
        sat_mock.return_value = {'ISS (ZARYA)':{}, 'COSMOS-123':{}}
        self.assertEqual(my_scheduler.get_satellites('Hello'), {'ISS (ZARYA)':{}, 'COSMOS-123':{}})
        visible_mock.return_value = {'ISS (ZARYA)':{}, 'COSMOS-123':{}}
        self.assertEqual(my_scheduler.get_visible_satellites(1,2,3), {'ISS (ZARYA)':{}, 'COSMOS-123':{}})
        (stime, satellites) = Scheduler.find_time(my_scheduler, cumulative=True)
        self.assertTrue(type(stime)==type(datetime.now()))
        self.assertTrue(satellites[0]=="ISS (ZARYA)" or "COSMOS-123")
        self.assertTrue(satellites[1]=="COSMOS-123" or "ISS (ZARYA)")
    
    @patch('scheduler.Scheduler.get_user_location_Topos')
    @patch('scheduler.Scheduler.find_time_interval_set')
    @patch('scheduler.Scheduler.get_satellites')
    @patch('scheduler.Scheduler.get_visible_satellites')
    def test_not_cumulative_find_time_WB013(self, visible_mock,sat_mock,time_mock, topos_mock):
        """
        TestID: Test_WB_013
        Testing and mocking for calling Schedule.find_time()
        """
        my_scheduler = Scheduler()
        topos_mock.return_value = 2
        self.assertEqual(my_scheduler.get_user_location_Topos(1), 2)
        time_mock.return_value = [(datetime(2017,4,10,2,2,20),datetime(2017,4,10,3,2,20))]
        self.assertEqual(my_scheduler.find_time_interval_set(1,2,3),[(datetime(2017,4,10,2,2,20),datetime(2017,4,10,3,2,20))])
        sat_mock.return_value = {'ISS (ZARYA)':{}, 'COSMOS-123':{}}
        self.assertEqual(my_scheduler.get_satellites('Hello'), {'ISS (ZARYA)':{}, 'COSMOS-123':{}})
        visible_mock.return_value = {'ISS (ZARYA)':{}, 'COSMOS-123':{}}
        self.assertEqual(my_scheduler.get_visible_satellites(1,2,3), {'ISS (ZARYA)':{}, 'COSMOS-123':{}})
        (stime, satellites) = Scheduler.find_time(my_scheduler, location=(80.000,-40.000))
        self.assertTrue(type(stime)==type(datetime.now()))
        for item in satellites:
            self.assertTrue(satellites[item]=="ISS (ZARYA)" or "COSMOS-123")
            
    def test_get_visible_satellites(self):
        """
        TestID: Test_WB_014
        Testing for calling Schedule.get_visible_satellites()
        """
        my_scheduler = Scheduler()
        load = my_scheduler._skyload
        ts_value = my_scheduler.ts
        value = my_scheduler.get_visible_satellites(ts_value.utc(2018,10,18,2,20,14), load.tle('http://celestrak.com/NORAD/elements/visual.txt'), Topos('40.000 N', '80.000 E'))
        self.assertEqual(type(value[0]), type('COSMOS 1455'))
       
if __name__=="__main__":
    unittest.main()
