"""
FIT-2107 Assignment3
Team Name: Tom and Jerry
Author 1: Thuta Lin(ID: 28729994)
Author 2: Giridhar Gopal Sharma(ID: )
Last Modified Time: 18/10/2018 
"""

'''A class to calculate optimal times for satellite spotting
Initial skeleton code written by Robert Merkel for FIT2107 Assignment 3
'''

from skyfield.api import Loader, Topos
import time
from datetime import datetime,timedelta

class IllegalArgumentException(Exception):
    '''An exception to throw if somebody provides invalid data to the Scheduler methods'''
    pass

class Scheduler:

    '''The class for calculating optimal satellite spotting times.  You can and should add methods
    to this, but please don't change the parameter list for the existing methods.  '''
    def __init__(self):
        '''Constructor sets things to put downloaded data in a sensible location. You can add
        to this if you want.  '''
        self._skyload = Loader('~/.skyfield-data')
        self.ts = self._skyload.timescale()
    
    def find_time(self, satlist_url='http://celestrak.com/NORAD/elements/visual.txt',
    start_time=datetime.now(), n_windows=24, duration=60, sample_interval=1, cumulative=False,
    location=(-37.910496,145.134021)):
        '''NOTE: this is the key function that you'll need to implement for the assignment.  Please
        don't change the arguments.
        arguments: satlist_url (string) a URL to a file containing a list of Earth-orbiting
        satellites in TLE format)
                      start_time: a Python Datetime object representing the
                      the start of the potential observation windows,return

                      duration: the size (in minutes) of an observation window - must be positive
                      n_windows: the number of observation windows to check.  Must be a positive integer
                      sample_interval: the interval (in minutes) at which the visible
                      satellites are checked.  Must be smaller than duration.
                      cumulative: a boolean to determine whether we look for the maximum number
                      of satellites visible at any time within the duration (if False), or the
                      cumulative number of distinct satellites visible over the duration (if True)
                      location: a tuple (lat, lon) of floats specifying he latitude and longitude of the
                      observer.  Negative latitudes specify the southern hemisphere, negative longitudes
                      the western hemisphere.  lat must be in the range [-90,90], lon must be in the
                      range [-180, 180]
        returns:a tuple ( interval_start_time, satellite_list), where start_interval is
        the time interval from the set {(start_time, start_time + duration),
        (start_time + duration, start_time + 2*duration)...} with the most satellites visible at some
        point in the interval, or the most cumulative satellites visible over the interval (if cumulative=True)
        See the assignment spec sheet for more details.
        raises: IllegalArgumentException if an illegal argument is provided'''
        
        if isinstance(satlist_url,str):
            pass
        else:
            raise TypeError
        
        if isinstance(n_windows, int)==False:
            raise TypeError
        elif n_windows <= 0:
            raise ValueError
        else:
            pass
            
        if isinstance(duration, int)==False:
            raise TypeError
        elif duration <= 0:
            raise ValueError
        else:
            pass

        # Exception handling for sample_interval
        if isinstance(sample_interval, int)==False:
            raise TypeError
        elif sample_interval <= 0:
            raise ValueError
        else:
            pass

        # Exception handling for cumulative
        if isinstance(cumulative, bool)==False:
            raise TypeError

        # Exception handling for location
        if isinstance(location, tuple):
            if isinstance(location[0], float)==False:
                raise TypeError
            elif location[0] > 90 or location[0] < -90:
                raise ValueError
            else:
                if isinstance(location[1], float)==False:
                    raise TypeError
                else:
                    if location[1] > 180 or location[1] < -180:
                        raise ValueError
        else:
            raise TypeError
        
        visible_satellites = []
        satellite_list = []
        max_num_sat_each_interval = []
        total_num_sat_each_interval = []

        # Function Calling
        user_location = self.get_user_location_Topos(location)
        #print(user_location)

        # Function Calling
        time_interval_set = self.find_time_interval_set(start_time, n_windows, duration)
        
        try:
            satellites = self.get_satellites(satlist_url)
        except Exception:
            raise IllegalArgumentException
        
        # Function Calling
        for item in time_interval_set:
            interval_start_time = item[0]
            interval_end_time = item[1]
            while(interval_start_time < interval_end_time):
                
                time_only = interval_start_time.time()
                date_only = interval_start_time.date()
                t = self.ts.utc(date_only.year, date_only.month, date_only.day, time_only.hour, time_only.minute, time_only.second)

                # get_visible_satellite() method calling
                visible_satellites.append(self.get_visible_satellites(t, satellites, user_location))
                interval_start_time = interval_start_time + timedelta(minutes=sample_interval)
                
            # Max satellite
            max_value = self.get_max_num_satellites(visible_satellites)
            max_num_sat_each_interval.append(max_value)
                
            # Union satellite
            union = self.get_union_num_visible_satellites(visible_satellites)
            total_num_sat_each_interval.append(union)

        if cumulative:
            satellite_list = self.get_max_num_satellites(total_num_sat_each_interval)
                    
        else:
            satellite_list = self.get_max_num_satellites(max_num_sat_each_interval)

        return (interval_start_time, satellite_list)
        

    def get_satellites(self, arg_url):
        """
        This function downloads and gets all the available satellites
        """
        satellites = self._skyload.tle(arg_url)
        return satellites
        

    def get_user_location_Topos(self, arg_location):
        """
        This function calculates user position
        """
        if arg_location[0] < 0:
            lat_location = str(abs(arg_location[0]))+" S"
        else:
            lat_location = str(arg_location[0])+" N"
            
        if arg_location[1] < 0:
            long_location = str(abs(arg_location[1]))+" W"
        else:
            long_location = str(arg_location[1])+" E"

        return Topos(lat_location, long_location)

    def find_time_interval_set(self, arg_start_time, arg_n_windows, arg_duration):
        """
        This function finds all the time interval
        """
        temp_time_interval = []
        for i in range(arg_n_windows):
            end_time = arg_start_time+timedelta(minutes=arg_duration)
            temp_time_interval.append((arg_start_time, end_time))
            arg_start_time = end_time
        return temp_time_interval

    def get_visible_satellites(self, arg_t, arg_satellites, arg_user_location):
        """
        This function finds the satellites visible to user based on position and time
        """
        visible_satellite_at_point = []
        for i in arg_satellites:
            difference = arg_satellites[i] - arg_user_location
            topocentric = difference.at(arg_t)
            alt, az, distance = topocentric.altaz()
            if alt.degrees > 0 and isinstance(i,str):
                visible_satellite_at_point.append(i)
    
        return visible_satellite_at_point

    def get_max_num_satellites(self, arg_satellites):
        """
        This function calculates the maximum number of satellites 
        """
        max_satellites = None
        max_value = 0
        for item in arg_satellites:
            if len(item)>max_value:
                max_value = len(item)
                max_satellites = item
        return max_satellites

    def get_union_num_visible_satellites(self, arg_satellites):
        """
        This function calculates the total number of satellites 
        """
        union_satellites = []
        for i in arg_satellites:
            union_satellites = list(set(union_satellites)|set(i))
        return union_satellites
    
if __name__ == '__main__':
    pass
